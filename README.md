# stylecheck

Additional style checkers to enforce style rules not supported by upstream Checkstyle.

## Usage

We (will be) on Maven Central. Usage is simple, simply add to the `checkstyle` configuration

```kotlin

plugins {
    checkstyle
}

repositories {
    mavenCentral()
}

dependencies {
    checkstyle("ca.stellardrift:stylecheck:0.1")
}
```

## contributing

Contributions are welcome. For any feature additions, please be in touch by filing an issue so we can confirm your request is in scope.

Any contributions must be released under the terms of the GPL v3 or later.
