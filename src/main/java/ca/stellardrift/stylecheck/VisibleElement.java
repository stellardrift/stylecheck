/*
 * stylecheck -- additional style checks for specialized checkstyle usage
 *
 * Copyright (C) 2020-2022, zml ['@'] stellardrift [dot] ca
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ca.stellardrift.stylecheck;

import com.puppycrawl.tools.checkstyle.api.DetailAST;
import com.puppycrawl.tools.checkstyle.api.TokenTypes;
import java.util.function.Predicate;

import static java.util.Objects.requireNonNull;

/**
 * A type of element that can have a visibility modifier applied.
 */
public enum VisibleElement implements Predicate<DetailAST> {
    CLASS(ast -> {
        int type = ast.getType();
        return (type == TokenTypes.CLASS_DEF || type == TokenTypes.INTERFACE_DEF
            || type == TokenTypes.ENUM_DEF || type == TokenTypes.ANNOTATION_DEF)
            && (ast.getParent() == null || ast.getParent().getType() != TokenTypes.SLIST) /* not a named local class*/;
    }),
    FIELD(ast -> ast.getType() == TokenTypes.VARIABLE_DEF
        && ast.getParent().getType() == TokenTypes.OBJBLOCK), // only for class variables (fields), not local variables
    METHOD(ast -> ast.getType() == TokenTypes.METHOD_DEF);

    private final Predicate<DetailAST> pred;

    /**
     * A predicate, to test on the parent of a {@link TokenTypes#MODIFIERS} node.
     *
     * @param pred predicate to test against
     */
    VisibleElement(final Predicate<DetailAST> pred) {
        this.pred = requireNonNull(pred, "pred");
    }

    @Override
    public boolean test(final DetailAST detailAST) {
        return this.pred.test(detailAST);
    }
}
