/*
 * stylecheck -- additional style checks for specialized checkstyle usage
 *
 * Copyright (C) 2020-2022, zml ['@'] stellardrift [dot] ca
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ca.stellardrift.stylecheck;

import com.puppycrawl.tools.checkstyle.StatelessCheck;
import com.puppycrawl.tools.checkstyle.api.AbstractCheck;
import com.puppycrawl.tools.checkstyle.api.DetailAST;
import com.puppycrawl.tools.checkstyle.api.TokenTypes;
import com.puppycrawl.tools.checkstyle.utils.TokenUtil;
import java.util.Arrays;
import java.util.Collections;
import java.util.EnumSet;
import java.util.Set;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Require that the visibility is explicitly specified, either with one of the
 * Java modifier keywords, or a {@code /* package *&sol; } comment.
 *
 * <p>Optionally also require that the visibility modifier is the declaration's
 * first non-annotation modifier.</p>
 */
@StatelessCheck
public class RequireExplicitVisibilityModifier extends AbstractCheck {
    private static final int[] VISIBILITY_MODIFIERS = new int[]{
        TokenTypes.LITERAL_PUBLIC, TokenTypes.LITERAL_PRIVATE, TokenTypes.LITERAL_PROTECTED};

    private static final String LITERAL_PACKAGE = "package";

    static {
        Arrays.sort(VISIBILITY_MODIFIERS);
    }

    @Override
    public int[] getDefaultTokens() {
        return new int[]{TokenTypes.MODIFIERS};
    }

    @Override
    public int[] getAcceptableTokens() {
        return new int[]{TokenTypes.MODIFIERS};
    }

    @Override
    public int[] getRequiredTokens() {
        return new int[]{TokenTypes.MODIFIERS};
    }

    @Override
    public boolean isCommentNodesRequired() {
        return true;
    }

    // Settings

    private final Set<VisibleElement> checkedElementTypes = EnumSet.noneOf(VisibleElement.class);
    // whether visibility modifiers have to be at the beginning of visibility declarations
    private boolean anchoredToBeginning = false;

    public void setCheckedElementTypes(final String... tokenNames) {
        for (final String elementType : tokenNames) {
            this.checkedElementTypes.add(VisibleElement.valueOf(elementType));
        }
    }

    public void setAnchoredToBeginning(final boolean anchoredToBeginning) {
        this.anchoredToBeginning = anchoredToBeginning;
    }

    @Override
    public void init() {
        if (this.checkedElementTypes.isEmpty()) {
            Collections.addAll(this.checkedElementTypes, VisibleElement.values());
        }
    }

    // the actual check //

    @Override
    public void visitToken(final DetailAST ast) {
        final DetailAST parent = ast.getParent();
        if (parent == null) {
            return;
        }

        final VisibleElement parentType = this.matchElement(parent);
        if (parentType == null || this.isInterfaceElement(parent)) {
            return; // ignore`
        }
        // -> /* package */ class Blah {
        if (ast.getFirstChild() == null) {
            if (ast.getNextSibling() != null && this.isPackageVisibilityComment(ast.getNextSibling(), false)) {
                return;
            }
        }

        boolean visibilityFound = false;
        DetailAST nonFirstFound = null;
        for (DetailAST child = ast.getFirstChild(); child != null; child = child.getNextSibling()) {
            if (Arrays.binarySearch(VISIBILITY_MODIFIERS, child.getType()) >= 0
                || this.isPackageVisibilityComment(child, false)) {
                if (visibilityFound) {
                  this.log(child, Messages.VISIBILITY_DUPLICATE_MODIFIER);
                }
                visibilityFound = true;
            } else if (this.anchoredToBeginning && !visibilityFound && nonFirstFound == null
                && child.getType() != TokenTypes.ANNOTATION
                && !TokenUtil.isCommentType(child.getType())) { // we've found a non-visibility modifier first (annotations + comments ok)
                nonFirstFound = child; // only log once
            }
        }
        if (!visibilityFound) {
            // Sometimes the package comment is a child of the TYPE token to the right of MODIFIERS instead,
            // or is itself to the right of MODIFIERS
            if (ast.getNextSibling() != null && (this.isPackageVisibilityComment(ast.getNextSibling(), true)
                || this.isPackageVisibilityComment(ast.getNextSibling().getFirstChild(), true))) {
                return;
            }
            this.log(ast, Messages.VISIBILITY_NO_MODIFIER);
        } else if (nonFirstFound != null) {
            this.log(nonFirstFound, Messages.VISIBILITY_NOT_FIRST, nonFirstFound.getText());
        }
    }

    private @Nullable VisibleElement matchElement(final @NotNull DetailAST parent) {
        for (final VisibleElement element : this.checkedElementTypes) {
            if (element.test(parent)) {
                return element;
            }
        }
        return null;
    }

    private boolean isInterfaceElement(@NotNull DetailAST parent) {
        while ((parent = parent.getParent()) != null) {
            if (VisibleElement.CLASS.test(parent)) {
                return parent.getType() == TokenTypes.INTERFACE_DEF;
            }
        }
        return false;
    }

    /**
     * Detect a package visibility comment and return the last ast node associated
     * with it.
     *
     * @param start the starting node, that will be of type
     * {@link TokenTypes#BLOCK_COMMENT_BEGIN} if it is a package visibility
     * modifier.
     * @return end of comment, or null if not a comment
     */
    private boolean isPackageVisibilityComment(@Nullable DetailAST start, final boolean siblings) {
        for (; start != null && start.getType() == TokenTypes.BLOCK_COMMENT_BEGIN; start = siblings ? start.getNextSibling() : null) {
            final DetailAST next = start.getFirstChild();
            if (next != null && next.getType() == TokenTypes.COMMENT_CONTENT
                && next.getText().trim().equals(LITERAL_PACKAGE)) {
                final DetailAST end = next.getNextSibling();
                return end != null && end.getType() == TokenTypes.BLOCK_COMMENT_END;
            }
        }
        return false;
    }
}
