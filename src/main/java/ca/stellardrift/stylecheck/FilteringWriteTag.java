/*
 * stylecheck -- additional style checks for specialized checkstyle usage
 *
 * Copyright (C) 2020-2022, zml ['@'] stellardrift [dot] ca
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ca.stellardrift.stylecheck;

import com.puppycrawl.tools.checkstyle.StatelessCheck;
import com.puppycrawl.tools.checkstyle.api.DetailAST;
import com.puppycrawl.tools.checkstyle.api.Scope;
import com.puppycrawl.tools.checkstyle.api.TokenTypes;
import com.puppycrawl.tools.checkstyle.checks.javadoc.WriteTagCheck;
import com.puppycrawl.tools.checkstyle.utils.AnnotationUtil;
import com.puppycrawl.tools.checkstyle.utils.ScopeUtil;
import java.util.HashSet;
import java.util.Set;

import static java.util.Objects.requireNonNull;

/**
 * {@link WriteTagCheck} that only operates on members with the minimum scope.
 *
 * <p>Additionally, this will ignore values with certain annotations,
 * by default {@link Override} and {@code Test}.</p>
 */
@StatelessCheck
public class FilteringWriteTag extends WriteTagCheck {

    private static final Set<String> DEFAULT_IGNORES = Set.of(
      "Override",
      "Test"
    );

    private boolean ignoredAnnotationsInitialized = false;
    private final Set<String> ignoredAnnotations = new HashSet<>();
    private Scope minimumScope = Scope.PROTECTED;

    /**
     * Add annotations to ignore. Can be qualified or unqualified.
     *
     * @param ignoreAnnotations Annotations to ignore
     */
    public void setIgnoredAnnotations(final String... ignoreAnnotations) {
        requireNonNull(ignoreAnnotations);
        this.ignoredAnnotationsInitialized = true;
        this.ignoredAnnotations.addAll(Set.of(ignoreAnnotations));
    }

    /**
     * Set the minimum scope to trigger this check on.
     *
     * <p>Any elements less visible than this scope will be ignored.</p>
     *
     * @param scope minimum scope
     */
    public void setMinimumScope(final Scope scope) { // checkstyle only implements a converter for this as an array
        requireNonNull(scope);
        this.minimumScope = scope;
    }

    @Override
    public void init() {
        super.init();
        if (!this.ignoredAnnotationsInitialized) {
            this.ignoredAnnotations.addAll(DEFAULT_IGNORES);
        }
    }

    @Override
    protected String getMessageBundle() {
        // superclass messages
        return Utils.getSuperclassBundle(this.getClass());
    }

    @Override
    public void visitToken(final DetailAST ast) {
        // Skip if annotated with any of the annotations we ignore.
        if (AnnotationUtil.containsAnnotation(ast, this.ignoredAnnotations)) {
            return;
        }

        // Don't do check if we are less visible than the minimum scope
        Scope scope = ScopeUtil.getScopeFromMods(ast.findFirstToken(TokenTypes.MODIFIERS));
        if (scope == Scope.PACKAGE) {
            // default visibility in interfaces and annotations is public
            if (ScopeUtil.isInInterfaceOrAnnotationBlock(ast)) {
                scope = Scope.PUBLIC;
            }
        }

        // If the surrounding scope is less than our declared scope, we are restricted to that scope anyways
        // (for example, if we are a public method in a package-private class)
        final Scope surrounding = ScopeUtil.getSurroundingScope(ast);
        if (surrounding != null && !surrounding.isIn(scope)) {
            scope = surrounding;
        }

        if (scope.compareTo(this.minimumScope) > 0) {
            return;
        }

        super.visitToken(ast);
    }
}
