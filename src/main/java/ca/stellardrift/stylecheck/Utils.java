/*
 * stylecheck -- additional style checks for specialized checkstyle usage
 *
 * Copyright (C) 2020-2022, zml ['@'] stellardrift [dot] ca
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ca.stellardrift.stylecheck;

import com.puppycrawl.tools.checkstyle.api.DetailAST;
import com.puppycrawl.tools.checkstyle.api.TokenTypes;
import com.puppycrawl.tools.checkstyle.utils.TokenUtil;

final class Utils {
  private static final String BUNDLE_NAME = "messages";

  private Utils() {
  }

  static String getSuperclassBundle(final Class<?> clazz) {
    final String packageName;
    if (clazz.getSuperclass() != null) {
      packageName = clazz.getSuperclass().getPackage().getName();
    } else {
      packageName = clazz.getPackage().getName();
    }
    if (packageName.isEmpty()) {
      return BUNDLE_NAME;
    } else {
      return packageName + '.' + BUNDLE_NAME;
    }
  }

  static String annotationString(final DetailAST ast) {
      if (ast.getType() != TokenTypes.ANNOTATION) {
          return "NOT AN ANNOTATION, was " + TokenUtil.getTokenName(ast.getType());
      }

      final StringBuilder builder = new StringBuilder();
      DetailAST pointer = ast.getFirstChild();
      while (pointer != null) {
          append(builder, pointer);
          pointer = pointer.getNextSibling();
      }

      return builder.toString();
  }

  static void append(final StringBuilder builder, final DetailAST ast) {
      switch (ast.getType()) {
          case TokenTypes.IDENT:
          case TokenTypes.AT:
              builder.append(ast.getText());
              break;
          case TokenTypes.DOT:
              DetailAST pointer = ast.getFirstChild();
              while (pointer != null) {
                  final boolean wasIdent = pointer.getType() == TokenTypes.IDENT;
                  builder.append(pointer.getText());
                  pointer = pointer.getNextSibling();
                  if (wasIdent && pointer != null) {
                      builder.append(".");
                  }
              }
              break;
      }
  }
}
