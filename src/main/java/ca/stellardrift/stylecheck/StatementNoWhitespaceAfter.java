/*
 * stylecheck -- additional style checks for specialized checkstyle usage
 *
 * Copyright (C) 2020-2022, zml ['@'] stellardrift [dot] ca
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ca.stellardrift.stylecheck;

import com.puppycrawl.tools.checkstyle.api.DetailAST;
import com.puppycrawl.tools.checkstyle.api.TokenTypes;
import com.puppycrawl.tools.checkstyle.checks.whitespace.NoWhitespaceAfterCheck;
import java.util.stream.IntStream;

/**
 * An extension of the {@link NoWhitespaceAfterCheck} that accepts more tokens.
 *
 * <p>These extra tokens are: {@link TokenTypes#LITERAL_IF},
 * {@link TokenTypes#LITERAL_FOR},
 * {@link TokenTypes#LITERAL_WHILE},
 * {@link TokenTypes#LITERAL_CATCH},
 * {@link TokenTypes#LITERAL_SWITCH},
 * and {@link TokenTypes#LITERAL_TRY}</p>
 *
 * <p>This check exists until <a href="https://github.com/checkstyle/checkstyle/issues/8312">the upstream
 * feature request</a> adding it has been addressed.</p>
 */
public class StatementNoWhitespaceAfter extends NoWhitespaceAfterCheck {

    @Override
    public int[] getAcceptableTokens() {
        return IntStream.concat(
            IntStream.of(super.getAcceptableTokens()),
            IntStream.of(TokenTypes.LITERAL_IF,
                TokenTypes.LITERAL_FOR,
                TokenTypes.LITERAL_WHILE,
                TokenTypes.LITERAL_CATCH,
                TokenTypes.LITERAL_SWITCH,
                TokenTypes.LITERAL_TRY)
        ).toArray();
    }

    @Override
    protected String getMessageBundle() {
        return Utils.getSuperclassBundle(this.getClass());
    }

    @Override
    public void visitToken(final DetailAST ast) {
        // exclude try-without-resources
        if (ast.getType() == TokenTypes.LITERAL_TRY) {
            if (ast.getFirstChild() == null
                || ast.getFirstChild().getType() != TokenTypes.RESOURCE_SPECIFICATION) {
                return;
            }
        }
        super.visitToken(ast);
    }
}
