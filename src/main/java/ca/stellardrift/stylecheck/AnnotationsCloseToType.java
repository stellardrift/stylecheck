/*
 * stylecheck -- additional style checks for specialized checkstyle usage
 *
 * Copyright (C) 2022-2023, zml ['@'] stellardrift [dot] ca
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ca.stellardrift.stylecheck;

import com.puppycrawl.tools.checkstyle.StatelessCheck;
import com.puppycrawl.tools.checkstyle.api.AbstractCheck;
import com.puppycrawl.tools.checkstyle.api.DetailAST;
import com.puppycrawl.tools.checkstyle.api.FullIdent;
import com.puppycrawl.tools.checkstyle.api.TokenTypes;
import java.util.HashSet;
import java.util.Set;
import org.jetbrains.annotations.NotNull;

import static java.util.Objects.requireNonNull;

/**
 * Require that certain annotations are treated as type-use annotations.
 *
 * <p>These annotations must be at the end of a list of modifiers and have no
 * newline between the modifier and the following type.</p>
 *
 * @since 0.2.0
 */
@StatelessCheck
public class AnnotationsCloseToType extends AbstractCheck {
    private static Set<String> DEFAULT_ANNOTATIONS = Set.of("NotNull", "NonNull", "Nonnull", "Nullable", "Pattern", "RegExp");
    private boolean typeUseAnnotationsInitialized = false;
    private final Set<String> typeUseAnnotations = new HashSet<>();

    @Override
    public @NotNull int[] getDefaultTokens() {
        return new int[] {TokenTypes.MODIFIERS};
    }

    @Override
    public @NotNull int[] getAcceptableTokens() {
        return this.getDefaultTokens();
    }

    @Override
    public @NotNull int[] getRequiredTokens() {
        return this.getDefaultTokens();
    }

    /**
     * Set the simple names of annotations that should be treated as type use annotations.
     *
     * @param annotations the annotations
     */
    public void setTypeUseAnnotations(final @NotNull String@NotNull... annotations) {
        requireNonNull(annotations);
        this.typeUseAnnotationsInitialized = true;
        this.typeUseAnnotations.addAll(Set.of(annotations));
    }

    @Override
    public void init() {
        super.init();
        if (!this.typeUseAnnotationsInitialized) {
            this.typeUseAnnotationsInitialized = true;
            this.typeUseAnnotations.addAll(DEFAULT_ANNOTATIONS);
        }
    }

    @Override
    public void visitToken(final DetailAST rootAST) {
        if (rootAST.getParent().getType() == TokenTypes.ANNOTATION_DEF) {
            // Don't apply to annotations on annotations themselves
            return;
        }

        // for children: go through for type-use annotations
        // if non-annotation: if any type use found, fail
        DetailAST firstAnnotation = null;
        DetailAST pointer = rootAST.getFirstChild();
        while (pointer != null) {
            if (pointer.getType() == TokenTypes.ANNOTATION && this.isMatchedAnnotation(pointer)) {
                if (firstAnnotation == null) {
                    firstAnnotation = pointer;
                }
            } else if (firstAnnotation != null) {
                this.log(pointer, Messages.TYPE_USE_ANNOTATION_AFTER_MODIFIERS, Utils.annotationString(firstAnnotation));
                break;
            }
            pointer = pointer.getNextSibling();
        }

        // then, check that the following type is on the same line:
        if (firstAnnotation != null) {
            final DetailAST right = rootAST.getNextSibling();

            if (right.getType() == TokenTypes.TYPE) {
                // make sure all type annotations are on the innermost class of any qualified references
                if (this.qualified(right)) {
                    this.log(firstAnnotation, Messages.TYPE_USE_ANNOTATE_NESTED_QUALIFIED, Utils.annotationString(firstAnnotation));
                }

                // ensure type is on same line as first type annotation
                if (right.getLineNo() != firstAnnotation.getLineNo()) {
                    this.log(right, Messages.TYPE_USE_TYPE_ON_SEPARATE_LINE, Utils.annotationString(firstAnnotation));
                }
            }
        }
    }

    private boolean qualified(final DetailAST ast) {
        DetailAST pointer = ast.getFirstChild();
        while (pointer != null) {
            if (pointer.getType() == TokenTypes.DOT) return true;

            pointer = pointer.getNextSibling();
        }

        return false;
    }

    private boolean isMatchedAnnotation(final DetailAST annotation) {
        // annotation skipping @, get simple name?
        final DetailAST at = annotation.getFirstChild();
        if (at.getType() != TokenTypes.AT) {
            return false;
        }

        final FullIdent ident = FullIdent.createFullIdent(annotation.getFirstChild().getNextSibling());
        return this.typeUseAnnotations.contains(ident.getText());
    }
}
