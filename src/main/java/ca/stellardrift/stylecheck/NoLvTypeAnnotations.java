/*
 * stylecheck -- additional style checks for specialized checkstyle usage
 *
 * Copyright (C) 2020-2022, zml ['@'] stellardrift [dot] ca
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ca.stellardrift.stylecheck;

import com.puppycrawl.tools.checkstyle.StatelessCheck;
import com.puppycrawl.tools.checkstyle.api.AbstractCheck;
import com.puppycrawl.tools.checkstyle.api.DetailAST;
import com.puppycrawl.tools.checkstyle.api.TokenTypes;
import org.jetbrains.annotations.NotNull;

/**
 * Block type annotations on local variables
 */
@StatelessCheck
public class NoLvTypeAnnotations extends AbstractCheck {
    @Override
    public int[] getDefaultTokens() {
        return new int[] {TokenTypes.VARIABLE_DEF};
    }

    @Override
    public int[] getAcceptableTokens() {
        return new int[] {TokenTypes.VARIABLE_DEF};
    }

    @Override
    public int[] getRequiredTokens() {
        return new int[] {TokenTypes.VARIABLE_DEF};
    }

    @Override
    public void visitToken(final @NotNull DetailAST start) {
        if (start.getParent() == null || start.getParent().getType() != TokenTypes.SLIST) {
            // we're not a local variable
            return;
        }

        // Recursively look for annotations and fail if any are found
        DetailAST pointer = start;
        outer: while (true) {
            // depth-first search: visit children, then siblings, step up
            if (pointer.getFirstChild() != null) {
                pointer = pointer.getFirstChild();
            } else {
                // Stop at assignment -- otherwise we traverse into local values
                while (pointer.getNextSibling() == null || pointer.getNextSibling().getType() == TokenTypes.ASSIGN) {
                    if ((pointer = pointer.getParent()) == start) {
                        break outer;
                    }
                }
                pointer = pointer.getNextSibling();
            }

            if (pointer.getType() == TokenTypes.ANNOTATION) {
                this.log(pointer, Messages.ANNOTATIONS_NO_TYPE_USE, pointer.findFirstToken(TokenTypes.IDENT).getText());
            }
        }

    }
}
